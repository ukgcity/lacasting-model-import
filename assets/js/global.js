jQuery(function ($) {

	$(document).ready(function($) {
		$('#lmi-model-importing').click(function(e) {
			e.preventDefault();

			lmi_model_importing();
		});
	});

	function lmi_model_importing() {
		if($('#lmi-id-to-import').val().length > 0) {
			$('#lmi-data-preloader').show();

			var data = {
				'action': 'import_action',
				'lmi-id-to-import': $('#lmi-id-to-import').val()
			};

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: ajax_object.ajax_url,
				data: data,
				success: function(response) {
					$('#lmi-data-preloader').hide();
					$('#lmi-answer-area').text('');
					$('#lmi-id-to-import').val('');

					for (var key in response) {
						$('#lmi-answer-area').append('<p>The resume of model ' + response[key].lacas_id + ' was added. See <a href="' + response[key].url + '" target="_blank">' + response[key].url + '</a>.</p>');
					}
				}
			});
		}
	}

});