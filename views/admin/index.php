<div class="wrap">
    <div id="lmi-data-preloader" style="display:none;">
        <span class="spinner"></span>
    </div>
    <h2>LACasting model importing</h2>

    <div id="lmi-answer-area"></div>

    <form method="post">
        <input type="hidden" name="lmi-form-action" value="F">


	<table class="form-table">
		<tr valign="top">
			<th scope="row">IDs of a model to import:</th>
			<td>
				<textarea id="lmi-id-to-import" name="lmi-id-to-import"></textarea>
				<p class="description">Specify the IDs of a model to import from LA Casting to the site (every ID should be separated by a comma).</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="#" id="lmi-model-importing" name="lmi-model-importing" class="button">Import</a>
			</td>
		</tr>
	</table>

    </form>
</div>