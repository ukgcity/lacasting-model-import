<?php
/**
 * @package LACasting-model-import
 */
/*
Plugin Name: LACasting-model-import
Plugin URI:
Description: LACasting-model-import is the plugin for an importing the data from LACasting.
Version: 1.0.0
Author: Yerbol O.
Author URI:
License: GPLv2 or later
Text Domain: lacasting-model-import
*/

require_once plugin_dir_path(__FILE__).'vendor/simple_html_dom.php';
require_once plugin_dir_path(__FILE__).'vendor/styles.php';


// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

add_action('admin_menu', 'register_lmi_custom_menu_page');

function register_lmi_custom_menu_page() {
	add_menu_page('LACasting importer', 'LACasting importer', 'manage_options', 'lacasting-model-import-option', 'lmi_custom_menu_page', plugins_url('assets/img/logo.png', __FILE__));
}

function lmi_custom_menu_page() {
	require_once plugin_dir_path(__FILE__).'/views/admin/index.php';
}

function lmi_siteorigin_panels_process_raw_widgets($widgets) {
	for ($i = 0; $i < count($widgets); $i++) {
		$info = isset($widgets[$i]['panels_info']) ? $widgets[$i]['panels_info'] : $widgets[$i]['info'];
		unset($widgets[$i]['info']);

		if (!empty($info['raw'])) {
			if (class_exists($info['class']) && method_exists($info['class'], 'update')) {
				$the_widget = new $info['class'];
				$widgets[$i] = $the_widget->update($widgets[$i], $widgets[$i]);
				unset($info['raw']);
			}
		}

		$widgets[$i]['panels_info'] = $info;
	}

	return $widgets;
}

function put_file($path, $content) {
	$file = $path;
	$current = $content;
	file_put_contents($file, $current);
}

function lmi_post_exists($id) {
	return is_string(get_post_status($id));
}

function lmi_get_post_id_by_meta($key, $value) {
	global $wpdb;

	$meta = $wpdb->get_results("SELECT * FROM ".$wpdb->postmeta." WHERE meta_key='".esc_sql($key)."' AND meta_value='".esc_sql($value)."'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
		$meta = $meta[0];
	}
	if (is_object($meta)) {
		return $meta->post_id;
	} else {
		return false;
	}
}

function lmi_get_post_id_by_guid($guid) {
	global $wpdb;

	$attachment = $wpdb->get_results("SELECT id FROM ".$wpdb->posts." WHERE guid like '%".esc_sql($guid)."%' AND post_type='attachment'");
	/*if (is_array($attachment) && !empty($attachment) && isset($attachment[0])) {
		$attachment = $attachment[0];
	}
	if (is_object($attachment)) {
		return $attachment->post_id;
	} else {
		return false;
	}*/
	if ($attachment) {
		return $attachment;
	} else {
		return false;
	}
}

function download_image($url) {
	$tmp = download_url($url);
	$file_array = array(
		'name' => basename($url),
		'tmp_name' => $tmp
	);

	// Check for download errors
	if (is_wp_error($tmp)) {
		@unlink($file_array['tmp_name']);
		return $tmp;
	}

	// Deleting the previous images
	$image_name = basename($url);
	$image_name = substr($image_name, 0, strrpos($image_name, "."));
        //$existing_attachment = get_page_by_title($image_name, OBJECT, 'attachment');
	//$existing_attachment = get_attachment_by_post_name($image_name);
	$attachments = lmi_get_post_id_by_guid($image_name);
	if($attachments) {
		foreach($attachments as $item) {
			//put_file('c:\work\wwwroot\\'.$image_name.'.log', $item->id.'/n');
			wp_delete_attachment($item->id);
		}
	}
	// Deleting the previous images

	$id = media_handle_sideload($file_array, 0);
	// Check for handle sideload errors.
	if (is_wp_error($id)) {
		@unlink($file_array['tmp_name']);
		return $id;
	}

	$attachment_url = wp_get_attachment_url($id);
	// Do whatever you have to here

	return $attachment_url;
}

function scraping_by_url($url) {
	$html = file_get_html($url);

	foreach($html->find('div.rep > ul.photoview > li.talentresult') as $app) {
		$item['id'] = $app->id;
		$ret[] = $item;
	}

	$html->clear();
	unset($html);

	return $ret;
}

function scraping_resume_by_url($url) {
	$html = file_get_html($url);

	$talent_type = '';

	if(count($html->find('div[id=resumeHeader]')) > 0) {
		$talent_type = 'ACTOR';
	} else if(count($html->find('div[id=coreInfo]')) > 0) {
		$talent_type = 'MODEL';
	}

	if($talent_type == 'ACTOR') {

		foreach($html->find('div[id=resumeHeader]') as $app) {
			$item['name'] = $app->find('div[id=nameUnions] > h1', 0)->innertext;
		}

		foreach($html->find('div[id=resumeContent]') as $app) {

			/* ----- ATTRIBUTES ----- */
			$attributes = '';
			foreach($app->find('div[id=statsArea] > div[id=attributes] > dl') as $atr) {
				$attributes .= $atr->innertext;
			}

			$item['attributes'] = $attributes;
			/* ----- ATTRIBUTES ----- */

			if (!$app->find('div[id=creditsArea] > div.resumeEmpty', 0)) {

				/* ----- SKILLS ----- */
				$skills_section   = '';
				$tpl_skills_title = '<tr class="skills"><td colspan="4"><dt>{skills_title}</dt></td></tr>';
				$tpl_skills_value = '<tr><td colspan="4">{skills_value}</td></tr>';
				$skills_section  .= '<tr class="skills"><td colspan="4"><h4 id="Skills">Skills</h4></td></tr>';

				foreach($app->find('div[id=creditsArea] tr.skills div.skills > dl') as $skills) {
					$skills_section .= str_replace('{skills_title}', $skills->find('dt', 0)->innertext, $tpl_skills_title);
					$skills_value = '';
					$skills_value_counta = 0;
					$skills_item_count = count($skills->find('dd > ul > li'));

					foreach($skills->find('dd > ul > li') as $skills_2) {
						$skills_value .= $skills_2->innertext;
						if (++$skills_value_counta != $skills_item_count) {
							$skills_value .= ', ';
						}
					}

					$skills_section .= str_replace('{skills_value}', $skills_value, $tpl_skills_value);
				}

				$item['skills'] = '<table>'.$skills_section.'</table>';
				/* ----- SKILLS ----- */

				/* ----- RESUME ----- */
				$resume_section = '';
				$resume_section = '<table>'.$app->find('div[id=creditsArea] > table', 0)->innertext.'</table>';
				$resume_section = str_get_html($resume_section);
				$resume_section->find('tr.skills', 0)->innertext = $skills_section;
				$item['resume'] = $resume_section;
				/* ----- RESUME ----- */

				/* ----- IMAGE ----- */
				foreach($app->find('div[id=mediaArea]') as $media) {
					foreach($media->find('img') as $img) {
						$img_name_arr = explode('/', str_replace('/M/', '/L/', $img->src));
						$img_name     = $img_name_arr[count($img_name_arr) - 1];
						$img_name     = substr($img_name, 0, strpos($img_name, '?'));

						$img_name_arr[count($img_name_arr) - 1] = $img_name;
						$img_url = implode('/', $img_name_arr);

						download_image($img_url);
					}
				}
				/* ----- IMAGE ----- */
			}
		}

	} else if($talent_type == 'MODEL') {

		foreach($html->find('div[id=coreInfo]') as $app) {
			$item['name'] = $app->find('div.TalentNameUnion > h1', 0)->innertext;
		}

		foreach($html->find('div[id=mainContent]') as $app) {

			/* ----- ATTRIBUTES ----- */
			$attributes = '';
			foreach($app->find('div[id=leftCol] > div[id=attributes] > dl') as $atr) {
				$attributes .= $atr->innertext;
			}

			$item['attributes'] = $attributes;
			/* ----- ATTRIBUTES ----- */

			if ($app->find('div[id=centralCol]', 0)) {

				/* ----- SKILLS ----- */
				$item['skills'] = '';
				/* ----- SKILLS ----- */

				/* ----- RESUME ----- */
				$item['resume'] = '';
				/* ----- RESUME ----- */

				/* ----- IMAGE ----- */
				foreach($app->find('div[id=mainImgContain] > div.media') as $media) {
					foreach($media->find('img') as $img) {
						$img_name_arr = explode('/', str_replace('/M/', '/L/', $img->src));
						$img_name     = $img_name_arr[count($img_name_arr) - 1];
						$img_name     = substr($img_name, 0, strpos($img_name, '?'));

						$img_name_arr[count($img_name_arr) - 1] = $img_name;
						$img_url = implode('/', $img_name_arr);

						download_image($img_url);
					}
				}

				if($app->find('div[id=centralCol] > div.media', 0)) {
				foreach($app->find('div[id=centralCol] > div.media') as $media) {
					foreach($media->find('img') as $img) {
						$img_name_arr = explode('/', str_replace('/M/', '/L/', $img->src));
						$img_name     = $img_name_arr[count($img_name_arr) - 1];
						$img_name     = substr($img_name, 0, strpos($img_name, '?'));

						$img_name_arr[count($img_name_arr) - 1] = $img_name;
						$img_url = implode('/', $img_name_arr);

						download_image($img_url);
					}
				}
				}
				/* ----- IMAGE ----- */
			}
		}

	}

	$ret[] = $item;
	$html->clear();
	unset($html);

	return $ret;
}

function import_model_function() {
	global $wpdb;

	$resume_permalinks = array();

        $resume_counter = 0;

	ini_set('user_agent', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');
	$lacasting_link = 'http://cni.lacasting.com/talent/profile/';

	$id_list_string = $_POST['lmi-id-to-import'];
	$id_list_string = str_replace(' ', '', $id_list_string);
	$id_list_arr    = explode(',', $id_list_string);

	foreach($id_list_arr as $item) {
		$res = scraping_resume_by_url($lacasting_link.$item);

		foreach($res as $res_item) {
			$data_panels_template = file_get_contents(plugin_dir_path(__FILE__) . '/tpl/template.json');


			// ----- IMAGES -----
			$images_ids = '';
			$images_ids_counta = 0;
			$thumbnail_id = 0;
			$images_arr = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'attachment' and post_title like '%" .$item. "%'");
			$images_ids_row_count = count($images_arr);
			foreach($images_arr as $images_row) {
				$images_ids .= $images_row->ID;
				if (++$images_ids_counta != $images_ids_row_count) {
					$images_ids.= ',';
				}

				if ($images_row->post_name == $item.'-0') {
					$thumbnail_id = $images_row->ID;
				}
			}

			if ($images_ids_counta == 1) {
				$images_ids.= ', '.$images_ids;
			}
			// ----- IMAGES -----


			// ----- ATTRIBUTES -----
			$attributes_string = '';
			$attributes_string = $res_item['attributes'];
			if(strlen($attributes_string) > 0) {
				$start_pos = strpos($attributes_string, '<dt>Gender</dt>') + strlen('<dt>Gender</dt><dd>');
				$end_pos   = strpos($attributes_string, '</dd>', $start_pos);
				$gender    = substr($attributes_string, $start_pos, $end_pos - $start_pos);

				$attributes_string = str_replace('</dt>', '</dt>|', $attributes_string);
				$attributes_string = str_replace('<dt>', '', $attributes_string);
				$attributes_string = str_replace('</dt>', '', $attributes_string);
				$attributes_string = str_replace('<dd>', '', $attributes_string);
				$attributes_string = str_replace("</dd>", "\n", $attributes_string);
			}
			// $attributes_string = htmlspecialchars($attributes_string, ENT_QUOTES);
			// ----- ATTRIBUTES -----


			// ----- RESUME -----
			$resume_string = '';
			$resume_string = $res_item['resume'];
			$resume_string = str_replace('	', '', $resume_string);
			// ----- RESUME -----


			$lmi_post_id = false;
			$lmi_post_exist_flag = false;
			$lmi_post_id = lmi_get_post_id_by_meta('lacasting_id', $item);
			if($lmi_post_id != false) {
				$lmi_post_exist_flag = lmi_post_exists($lmi_post_id);
			}

			// ----- INSERT OR UPDATE POST -----
			if($lmi_post_exist_flag == false) {
				$my_post = array(
					'post_title' => $res_item['name'],
					'post_content' => '',
					'post_status' => 'publish',
					'post_author' => 1,
					'post_type' => 'portfolio'
				);

				$post_id = wp_insert_post($my_post);
			} else {
				$post_id = $lmi_post_id;
				$my_post = array(
					'ID' => $post_id,
					'post_title' => $res_item['name'],
					'post_content' => '',
					'post_status' => 'publish',
					'post_author' => 1,
					'post_type' => 'portfolio'
				);
				wp_update_post($my_post);
			}

			$resume_permalinks[$resume_counter]['lacas_id'] = $item;
			$resume_permalinks[$resume_counter]['url'] = get_permalink($post_id);
			// ----- INSERT OR UPDATE POST -----


			// ----- SET CATEGORY -----
			$portfolio_category = get_term_by('name', $gender, 'portfolio_category');
			$term_taxonomy_ids  = wp_set_object_terms($post_id, $portfolio_category->term_id, 'portfolio_category', true);
			// ----- SET CATEGORY -----


			// ----- SET IMAGES TO CATEGORY -----

/*wp_insert_term(
  'ALEXITO', // the term 
  'media_category', // the taxonomy
  array(
    'description'=> 'ALEXITO',
    'slug' => 'alexito',
    'parent'=> $portfolio_category->term_id
  )
);*/


			$data_panels                                       = json_decode(wp_unslash($data_panels_template) , true);
			$data_panels['widgets'][2]['text']                 = $resume_string;
			$data_panels['widgets'][0]['upload_images_ids']    = $images_ids;
			$data_panels['widgets'][1]['model_details_values'] = $attributes_string;

			// DEBUGGING
			/* put_file(plugin_dir_path(__FILE__) . 'data_panels.txt', $resume_string);*/

			$data_panels['widgets'] = lmi_siteorigin_panels_process_raw_widgets($data_panels['widgets']);
			$data_panels            = lmi_siteorigin_panels_styles_sanitize_all($data_panels);


			// ----- UPDATE POST META -----
			update_post_meta($post_id, 'panels_data', $data_panels);
			update_post_meta($post_id, 'select_full_bg_type', 'single_bg_image');
			update_post_meta($post_id, 'full_screen_bg_transition', '6');
			update_post_meta($post_id, 'full_screen_auto_play', '0');
			update_post_meta($post_id, 'full_screen_slider_pause', '3000');
			update_post_meta($post_id, 'image_slide_title', '0');
			update_post_meta($post_id, 'disable_mid_container_bg', '0');
			update_post_meta($post_id, 'pf_link_new_window', '0');
			update_post_meta($post_id, 'relatedpost', '0');
			update_post_meta($post_id, 'list_images', 'slider');
			update_post_meta($post_id, 'kaya_pagesidebar', 'full');
			update_post_meta($post_id, 'post_views_count', '0');
			update_post_meta($post_id, 'sbg_selected_sidebar', 'a:1:{i:0;s:1:"0";}');
			update_post_meta($post_id, 'sbg_selected_sidebar_replacement', 'a:1:{i:0;s:7:"sidebar";}');
			update_post_meta($post_id, '_thumbnail_id', $thumbnail_id);
			update_post_meta($post_id, 'lacasting_id', $item);
			// ----- UPDATE POST META -----
		}

		$resume_counter++;
	}

	return $resume_permalinks;
	//return $lmi_post_exist_flag;
}



add_action('admin_enqueue_scripts', 'admin_import_enqueue');

function admin_import_enqueue($hook) {
	if('toplevel_page_lacasting-model-import-option' != $hook) {
		// Only applies to dashboard panel
		return;
	}

	wp_enqueue_style('lmi-style-css', plugins_url('/assets/css/style.css', __FILE__ ));
	wp_enqueue_script('ajax-script', plugins_url('/assets/js/global.js', __FILE__ ), array('jquery'));

	// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
	wp_localize_script('ajax-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));
}

// Same handler function...
add_action('wp_ajax_import_action', 'import_action_callback');

function import_action_callback() {
	global $wpdb;

	echo json_encode(import_model_function());
	wp_die();
}